from django.urls import path
from .import views

app_name = "todo_app"

urlpatterns = [
    path("", views.todos, name="todos"),
    path("delete/<int:todo_id>", views.delete_todo, name="delete"),
    path('update/<int:todo_id>', views.update_todo, name="update")
]

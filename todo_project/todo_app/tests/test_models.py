from django.test import TestCase, SimpleTestCase
from django.contrib.auth.models import User
from todo_app.models import Todo

USERNAME = 'mag'
PASSWORD = '123'


class TestUnitTodoModel(SimpleTestCase):
    """
    Unit tests for the Todo model
    """

    def test_update_method(self):
        """
        Checking to see if the update_method correctly updates the completed property
        """
        user = User(username = USERNAME, password = PASSWORD)
        todo = Todo(user = user, description = 'Get food')
        self.assertEqual(todo.completed, False)
        todo.update_completed()
        self.assertEqual(todo.completed, True)

class TestTodoModel(TestCase):
    """
    Test class to test if the model works properly
    """

    def setUp(self):
        """
        Creating a user in the temp database used to create a todo later
        """
        User.objects.create(username=USERNAME, password=PASSWORD)

    def test_todo_model(self):
        """
        Testing todo model
        """
        user = User.objects.get(username=USERNAME)
        todo = Todo(user=user, description='Get food')
        self.assertEqual(todo.description, 'Get food')
        self.assertEqual(todo.user, user)
        self.assertEqual(todo.completed, False)

        todo.completed = True
        self.assertEqual(todo.completed, True)

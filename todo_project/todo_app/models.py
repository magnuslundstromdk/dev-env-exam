from django.db import models
from django.contrib.auth.models import User


class Todo(models.Model):
    """
    Represents a todo entity
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    description = models.TextField()
    completed = models.BooleanField(default=False)

    def update_completed(self):
        """
        Updates the completed property to be the opposite
        """
        self.completed = not self.completed
